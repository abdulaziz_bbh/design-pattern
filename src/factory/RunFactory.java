package factory;

import factory.factoryMethod.DisplayService;
import factory.factoryMethod.XMLParser;
import factory.factoryMethod.display_services.FeedbackXMLDisplayService;
import factory.simpleFactory.Coffee;
import factory.simpleFactory.CoffeeType;

public class RunFactory {
    public static void main(String[] args) {

//        System.out.println(Coffee.create(CoffeeType.LATTE));

        DisplayService
                displayService =  new FeedbackXMLDisplayService();
        displayService.display();
    }
}
