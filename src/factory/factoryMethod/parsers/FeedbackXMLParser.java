package factory.factoryMethod.parsers;

import factory.factoryMethod.XMLParser;

public class FeedbackXMLParser implements XMLParser {
    @Override
    public String parse() {
        System.out.println("Parsing feedback XML...");
        return "Feedback XML Message";
    }
}
