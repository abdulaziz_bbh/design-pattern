package factory.factoryMethod;

public interface XMLParser {

    String parse();
}
