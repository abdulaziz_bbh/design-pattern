package factory.factoryMethod.display_services;

import factory.factoryMethod.XMLParser;
import factory.factoryMethod.DisplayService;
import factory.factoryMethod.parsers.ErrorXMLParser;

public class ErrorXMLDisplayService extends DisplayService {

    @Override
    protected XMLParser getParser() {
        return new ErrorXMLParser();
    }
}
