package adapter.site;

import adapter.payd.PayD;
import adapter.xpay.XPay;

public class RunAdapterExample {
    public static void main(String[] args) {
        XPay xpay = new XPayImpl();
        xpay.setCreditCardNo("4789565874102365");
        xpay.setCustomerName("Max Warner");
        xpay.setCardExpMonth("09");
        xpay.setCardExpYear("25");
        xpay.setCardCVVNo((short)235);
        xpay.setAmount(2565.23);

        PayD payD = new XpayToPayDAdapter(xpay);

        payDTest(payD);
    }

    public static void payDTest(PayD payD){
        System.out.println(payD.getCardOwnerName());
        System.out.println(payD.getCustCardNo());
        System.out.println(payD.getCVVNo());
        System.out.println(payD.getCardExpMonthDate());
        System.out.println(payD.getTotalAmount());
    }
}
