package factory.factoryMethod.parsers;

import factory.factoryMethod.XMLParser;

public class OrderXMLParser implements XMLParser {
    @Override
    public String parse() {
        System.out.println("Parsing order XML...");
        return "Order XML Message";
    }
}
