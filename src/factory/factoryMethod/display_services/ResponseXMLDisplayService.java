package factory.factoryMethod.display_services;

import factory.factoryMethod.DisplayService;
import factory.factoryMethod.XMLParser;
import factory.factoryMethod.parsers.ResponseXMLParser;

public class ResponseXMLDisplayService extends DisplayService {
    @Override
    protected XMLParser getParser() {
        return new ResponseXMLParser();
    }
}
