package factory.factoryMethod.display_services;

import factory.factoryMethod.DisplayService;
import factory.factoryMethod.XMLParser;
import factory.factoryMethod.parsers.OrderXMLParser;

public class OrderXMLDisplayService extends DisplayService {
    @Override
    protected XMLParser getParser() {
        return new OrderXMLParser();
    }
}
