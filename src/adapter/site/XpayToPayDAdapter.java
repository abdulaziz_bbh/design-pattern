package adapter.site;


import adapter.payd.PayD;
import adapter.xpay.XPay;

public class XpayToPayDAdapter implements PayD {

    private String custCardNo;
    private String cardOwnerName;
    private String cardExpMonthDate;
    private Integer cVVNo;
    private Double totalAmount;

    private final XPay xPay;

    public XpayToPayDAdapter(XPay xPay){
        this.xPay = xPay;
        setProp();
    }

    @Override
    public String getCustCardNo() {
        return custCardNo;
    }

    @Override
    public void setCustCardNo(String custCardNo) {
        this.custCardNo = custCardNo;
    }

    @Override
    public String getCardOwnerName() {
        return cardOwnerName;
    }

    @Override
    public void setCardOwnerName(String cardOwnerName) {
        this.cardOwnerName = cardOwnerName;
    }

    @Override
    public String getCardExpMonthDate() {
        return cardExpMonthDate;
    }

    @Override
    public void setCardExpMonthDate(String cardExpMonthDate) {
        this.cardExpMonthDate = cardExpMonthDate;
    }

    @Override
    public Integer getCVVNo() {
        return cVVNo;
    }

    @Override
    public void setCVVNo(Integer cVVNo) {
        this.cVVNo = cVVNo;
    }

    @Override
    public Double getTotalAmount() {
        return totalAmount;
    }

    @Override
    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public void setProp(){
        setCardOwnerName(this.xPay.getCustomerName());
        setCVVNo(this.xPay.getCardCVVNo().intValue());
        setCustCardNo(this.xPay.getCreditCardNo());
        setTotalAmount(this.xPay.getAmount());
        setCardExpMonthDate(this.xPay.getCardExpMonth() +"/"+ this.xPay.getCardExpYear());
    }
}
