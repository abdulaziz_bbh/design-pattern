package factory.factoryMethod;

public abstract class DisplayService {

    public void display(){
        XMLParser parser = getParser();
        String message = parser.parse();
        System.out.println(message);
    }
    protected abstract XMLParser getParser();
}
