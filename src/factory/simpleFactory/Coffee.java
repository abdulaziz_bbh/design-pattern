package factory.simpleFactory;

import java.util.ArrayList;
import java.util.List;

public class Coffee {

    public CoffeeType coffeeType;
    public List<CoffeeParameters> parameters;

    public Coffee(List<CoffeeParameters> coffeeParameters) {
        this.parameters = coffeeParameters;
    }


    public static Coffee create(CoffeeType coffeeType){
        switch (coffeeType){
            case LATTE -> {
                return newLate();
            }
            case AMERICANO -> {
                return newAmericano();
            }
        }
        throw new RuntimeException("Yoq");
    }

    private static Coffee newLate(){
        List<CoffeeParameters> coffeeParameters = new ArrayList<>();
        coffeeParameters.add(new CoffeeParameters(IngredientType.ESPRESSO, 1, 1));
        coffeeParameters.add(new CoffeeParameters(IngredientType.WATER, 2,2));
        return new Coffee(coffeeParameters);
    }
    public static Coffee newAmericano(){
        List<CoffeeParameters> coffeeParameters = new ArrayList<>();
        coffeeParameters.add(new CoffeeParameters(IngredientType.MILK_FOAM, 1, 1));
        coffeeParameters.add(new CoffeeParameters(IngredientType.CHOCOLATE, 2,2));
         return new Coffee(coffeeParameters);
    }

    @Override
    public String toString() {
        return "Coffee{" +
                "coffeeType=" + coffeeType +
                ", parameters=" + parameters +
                '}';
    }
}
