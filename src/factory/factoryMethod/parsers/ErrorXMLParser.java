package factory.factoryMethod.parsers;

import factory.factoryMethod.XMLParser;

public class ErrorXMLParser implements XMLParser {
    @Override
    public String parse() {
        System.out.println("Parsing error XML...");
        return "Error XML Message";
    }
}
