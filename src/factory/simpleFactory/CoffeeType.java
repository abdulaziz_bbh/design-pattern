package factory.simpleFactory;

public enum CoffeeType {

    AMERICANO,
    CAPPUCCINO,
    ESPRESSO,
    FLAT_WHITE,
    LATTE,
    MACHINATION,
    MOCHA

}
