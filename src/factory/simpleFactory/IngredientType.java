package factory.simpleFactory;

public enum IngredientType {

    MILK_FOAM,
    STEAMED_MILK,
    CHOCOLATE,
    ESPRESSO,
    WATER
}
