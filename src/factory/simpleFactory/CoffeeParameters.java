package factory.simpleFactory;


public class CoffeeParameters {

    public IngredientType ingredientType;
    public double count;

    public int order;


    public CoffeeParameters(IngredientType ingredientType, double count, int order) {
        this.ingredientType = ingredientType;
        this.count = count;
        this.order = order;
    }
    @Override
    public String toString() {
        return "CoffeeParameters{" +
                "ingredientType=" + ingredientType +
                ", count=" + count +
                ", order=" + order +
                '}';
    }

}
