package factory.factoryMethod.display_services;

import factory.factoryMethod.XMLParser;
import factory.factoryMethod.DisplayService;
import factory.factoryMethod.parsers.FeedbackXMLParser;

public class FeedbackXMLDisplayService extends DisplayService {
    @Override
    protected XMLParser getParser() {
        return new FeedbackXMLParser();
    }
}
